# Author: Lianghao Cao
# Date: 02/20/2022
from __future__ import absolute_import, division, print_function

import dolfin as dl
import hippylib as hl
import math
import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.append("../")
from OKNewton import *

# plt.rc('text', usetex=True) #Use for latex font in figures
# plt.rc('font', family='serif')

STATE = 0
PARAMETER = 1

# Defining the 3D polymer-substrate interaction field
class Substrate3D(dl.UserExpression):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def set_parameters(self, param):
        self.d_s_square = 12 * math.sqrt(3) * param[4] ** 2 * param[0] / math.sqrt(param[1] * (1 - param[2] ** 2))

    def eval(self, values, x):
        for i in range(4):
            if 1.9 + 10 * i <= x[0] <= 8.1 + 10 * i:
                values[0] = math.exp(-x[2] ** 2 / (2 * self.d_s_square))

    def value_shape(self):
        return ()

# Defining the 2D polymer-substrate interaction field
class Substrate2D(dl.UserExpression):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def set_parameters(self, param):
        pass

    def eval(self, values, x):
        for i in range(4):
            if 1.9 + 10 * i <= x[0] <= 8.1 + 10 * i:
                values[0] = 1.0

    def value_shape(self):
        return ()

if __name__ == "__main__":

    out_folder = "./results/" #output folder for storage.

    # 100 x 100 mesh of a square domain
    # 3D domain:
    # mesh = dl.BoxMesh(dl.Point(0.0, 0.0, 0.0), dl.Point(40, 25, 5), 80, 50, 10)

    #2D domain
    mesh = dl.RectangleMesh(dl.Point(0.0, 0.0), dl.Point(40, 25), 120, 75)

    # Create linear Lagrange finite element
    P1 = dl.FiniteElement("Lagrange", mesh.ufl_cell(), 1)

    # Create the mixed space for the state space
    Vh_STATE = dl.FunctionSpace(mesh, P1*P1)

    # Create "the real number space" for the model parameters
    param_dim = 5
    Vh_PARAMETER = dl.VectorFunctionSpace(mesh, "R", degree=0, dim=param_dim)

    # Create the order parameter space for order parameters
    Vu_sub = Vh_STATE.extract_sub_space([0]).collapse()

    Vh = [Vh_STATE, Vh_PARAMETER]

    # Create the initial random state object
    scale = 0.5
    correlation_length = 0.1
    state_init = GRF_initial_guess(Vh[STATE], correlation_length, scale)

    # Create the polymer-substrate interaction field object
    # substrate = Substrate3D()
    substrate = Substrate2D()

    # Create the forward problem object
    save = False #save the order parameter at each iteration. CAUTION! CAN TAKE UP A LOT OF SPACE!
    iter_solve = False #Enable iterative solver when the preconditioner is applicable. CAUTION! CAN LEAD TO DIVERGENCE IF ENABLED!
    pde = ok_min_prob(Vh, state_init, substrate = substrate, \
                      save=save, out_folder=out_folder, iter_solve = iter_solve)

    # Control the printing of the iteration data:
    # 1: Print information on screen
    # 2: Save data and visualization
    # <1: No printing
    pde.ok_min_solver.parameters["print_level"] = 2

    # Setting the number of double well backtracking iteration:
    pde.ok_min_solver.parameters["rescale_iterations"] = 3 # gamma = [1, 0.5, 0]

    # Setting the absolution tolerance for the residual norm:
    pde.ok_min_solver.parameters["abs_tolerance"] = 1e-10

    # Setting number of maximum iteration before giving up
    pde.ok_min_solver.parameters["maximum_iterations"] = 1000

    # Solve a particular problem
    x = [pde.generate_state(), pde.generate_parameter()]
    param = np.array([0.237, 1.68, 0.0, 0.25]) #(\epsolon, \sigma, m, \eta) for 2D domain
    # param = np.array([0.237, 1.68, 0.0, 0.15, 0.75]) #(\epsolon, \sigma, m, \eta, c_s) for 3D domain
    x[PARAMETER].set_local(param)
    pde.solveFwd(x[STATE], x)

    # Extract the order parameter
    op = dl.Function(Vu_sub)
    pde.extract_order_parameter(op.vector(), x[STATE])

    # Plot the order parameter in .vtu format
    file = dl.File(out_folder + 'order_param.pvd')
    file << op

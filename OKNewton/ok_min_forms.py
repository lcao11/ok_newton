from __future__ import absolute_import, division, print_function

import hippylib as hl
import numpy as np
import dolfin as dl
import math
from .misc import poisson_solve

STATE = 0
PARAMETER = 1

dl.parameters['linear_algebra_backend'] = 'PETSc'
dl.parameters["form_compiler"]["optimize"]     = True
dl.parameters["form_compiler"]["cpp_optimize"] = True
dl.parameters["form_compiler"]["representation"] = "uflacs"
dl.parameters["form_compiler"]["cpp_optimize_flags"] = "-O3 -ffast-math -march=native"


class ok_min_forms:
    def __init__(self, Vh, substrate, save, out_folder, iter_solve):

        self.Vh = Vh
        self.Vu_sub = Vh[STATE].extract_sub_space([0]).collapse()
        self._param_dim = self.Vh[PARAMETER].dim,()
        self._save = save
        self._substrate = substrate
        if not self._substrate is None:
            self._substrate_func = dl.Function(self.Vu_sub)

        self.iter_solve = iter_solve
        self.out_folder = out_folder

        [self._u, self._m] = [dl.Function(Vh[i]) for i in range(2)]
        p = dl.TestFunction(Vh[STATE])
        
        self._du = dl.Function(self.Vh[STATE])
        self._u2_solve_rhs = dl.Function(self.Vh[STATE])

        # build Msolver to compute gradient norm
        u_trial = dl.TrialFunction(Vh[STATE])
        self._u1_trial, u2_trial = dl.split(u_trial)
        self._p1, self._p2 = dl.split(p)
        self._MK = dl.PETScMatrix(Vh[STATE].mesh().mpi_comm())
        dl.assemble((self._u1_trial*self._p1 + u2_trial*self._p2\
                     + dl.inner(dl.grad(self._u1_trial), dl.grad(self._p1))\
                     + dl.inner(dl.grad(u2_trial), dl.grad(self._p2)))*dl.dx, tensor = self._MK)
        if iter_solve:
            self._solver4gnorm = dl.PETScKrylovSolver("cg", "icc")
            self._solver4gnorm.set_operator(self._MK)
            self._solver4gnorm.parameters["relative_tolerance"] = 1.e-12
            self._solver4gnorm.parameters["absolute_tolerance"] = 1.e-12
        else:
            self._solver4gnorm = dl.PETScLUSolver(Vh[STATE].mesh().mpi_comm(), self._MK)
            self._solver4gnorm.parameters['symmetric'] = True
        
        # build matrix to assemble newton step rhs
        [self._M_11, self._M_12, self._M_21, self._M_22] = [dl.PETScMatrix(Vh[STATE].mesh().mpi_comm()) for i in range(4)]
        dl.assemble(self._u1_trial*self._p1*dl.dx, tensor = self._M_11)
        dl.assemble(u2_trial*self._p2*dl.dx, tensor = self._M_22)
        dl.assemble(u2_trial*self._p1*dl.dx, tensor = self._M_12)
        dl.assemble(self._u1_trial*self._p2*dl.dx, tensor = self._M_21)
        
        [self._K_11, self._K_12, self._K_21, self._K_22] = [dl.PETScMatrix(Vh[STATE].mesh().mpi_comm()) for i in range(4)]
        dl.assemble(dl.inner(dl.grad(self._u1_trial), dl.grad(self._p1))*dl.dx, tensor = self._K_11)
        dl.assemble(dl.inner(dl.grad(u2_trial), dl.grad(self._p1))*dl.dx, tensor = self._K_12)
        dl.assemble(dl.inner(dl.grad(self._u1_trial), dl.grad(self._p2))*dl.dx, tensor = self._K_21)
        dl.assemble(dl.inner(dl.grad(u2_trial), dl.grad(self._p2))*dl.dx, tensor = self._K_22)

        if not self._substrate is None:
            self._M_11_ss = dl.PETScMatrix(Vh[STATE].mesh().mpi_comm())
            self._M_21_ss = dl.PETScMatrix(Vh[STATE].mesh().mpi_comm())

        # build vector for integration for u_1 in Vh[STATE]:
        self._u1_c = dl.assemble(dl.Constant(1.0)*self._p1*dl.dx)
        self._u2_c = dl.assemble(dl.Constant(1.0) * self._p2 * dl.dx)
        
        self._help = self.generate_state()
        
        # save data in file
        if self._save:
            self._file_u1 = dl.File(out_folder + "u1.pvd", "compressed")

        self._u2 = dl.Function(self.Vu_sub)
        self._func_sub = dl.Function(self.Vu_sub)
        self._solver4poisson = poisson_solve(Vh[STATE])
        self._help_sub = self.generate_sub_state()

        # build solver to solve u2 from given u1
        u_trial = dl.TrialFunction(self.Vu_sub)
        p = dl.TestFunction(self.Vu_sub)
        self._M_sub = dl.PETScMatrix(Vh[STATE].mesh().mpi_comm())
        self._K_sub = dl.PETScMatrix(Vh[STATE].mesh().mpi_comm())
        dl.assemble(u_trial*p*dl.dx, tensor = self._M_sub)
        dl.assemble(dl.inner(dl.grad(u_trial), dl.grad(p))*dl.dx, tensor = self._K_sub)
        if iter_solve:
            self._solver4u2 = dl.PETScKrylovSolver("cg", "icc")
            self._solver4u2.set_operator(self._M_sub)
            self._solver4u2.parameters["relative_tolerance"] = 1.e-12
            self._solver4u2.parameters["absolute_tolerance"] = 1.e-12

        else:
            self._solver4u2 = dl.PETScLUSolver(Vh[STATE].mesh().mpi_comm(), self._M_sub)
            self._solver4u2.parameters['symmetric'] = True
        
        self._u_sub_c = dl.assemble(dl.Constant(1.0)*p*dl.dx)
        self._vol = dl.assemble(dl.Constant(1.0)*dl.dx(Vh[STATE].mesh()))
                
        # storage for the hessian
        self._H_nln_quad = dl.PETScMatrix(Vh[STATE].mesh().mpi_comm())
        self._H_nln_lin = dl.PETScMatrix(Vh[STATE].mesh().mpi_comm())
        self._H_linear_sym = None
        self._H_linear_asym = None
        self.H = None
        self.P = None
        
        #storage for nonlinear vector
        self._d_nln_quad = self.generate_state()
        self._d_nln_cub = self.generate_state()
        
        # storage for the gradient
        self._gradient = self.generate_state()

        # Ohta Kawasaki potential
        if not substrate is None:
            self.ok_energy = np.ones(4)
        else:
            self.ok_energy = np.ones(3)

    def set_parameters(self, m):
        
        self._m.vector().zero()
        self._m.vector().axpy(1, m)

        self.eps = self._m.vector()[0]
        self.sigma = self._m.vector()[1]
        self.u1_mean = self._m.vector()[2]
        if not self._substrate is None:
            self.eta = self._m.vector()[3]
            self._substrate.set_parameters(self._m.vector())
            self._substrate_func.interpolate(self._substrate)
            self._assemble_substrate()

    def generate_state(self):

        return dl.Function(self.Vh[STATE]).vector()
    
    def generate_sub_state(self):

        return dl.Function(self.Vu_sub).vector()
    
    def _assign_vector(self, u_copy, u):
        u_copy.zero()
        u_copy.axpy(1., u)
        
    # define the PDE
    def varf_handler(self, u, m, p):
    
        (u1, u2) = dl.split(u)
        (p1, p2) = dl.split(p)

        (m0,m1,m2, m3) = dl.split(m)

        L0 = dl.inner(dl.grad(u2), dl.grad(p1)) * dl.dx + m1 * (u1 - m2) * p1 * dl.dx
        if not self._substrate is None:
            L1 = u2 * p2 * dl.dx + (1 - u1**2) * u1 * p2 * dl.dx - 0.75*m3*self._substrate_func*(u1**2 - 1) - \
                m0**2 * dl.inner(dl.grad(u1), dl.grad(p2)) * dl.dx
        else:
            L1 = u2 * p2 * dl.dx + (1 - u1**2) * u1 * p2 * dl.dx - \
                m0**2 * dl.inner(dl.grad(u1), dl.grad(p2)) * dl.dx

        L = L0 + L1

        return L
        
    def _set_linear_operator_sym(self, eps, sig):

        self._H_linear_sym = self._M_21.copy()
        self._H_linear_sym.axpy(1., self._M_12, False)
        self._H_linear_sym.axpy(1./sig, self._K_22, False)
        self._H_linear_sym.axpy(-eps**2, self._K_11, False)

    def _set_linear_operator_asym(self, eps, sig):

        self._H_linear_asym = self._M_11.copy()
        self._H_linear_asym.axpy(1., self._M_22, False)
        self._H_linear_asym.axpy(1./sig, self._K_12, False)
        self._H_linear_asym.axpy(-eps**2, self._K_21, False)
            
    def set_nonlinear_operator(self, u_vec = None, sym = True):
    
        if not u_vec is None:
            self._assign_vector(self._u.vector(), u_vec)
        (u1, u2) = dl.split(self._u)
        if sym:
            dl.assemble(u1**2*self._u1_trial*self._p1*dl.dx, tensor = self._H_nln_quad)
            if not self._substrate is None:
                dl.assemble(u1*self._substrate_func*self._u1_trial*self._p1*dl.dx, tensor = self._H_nln_lin)
        else:
            dl.assemble(u1**2*self._u1_trial*self._p2*dl.dx, tensor = self._H_nln_quad)
            if not self._substrate is None:
                dl.assemble(u1*self._substrate_func*self._u1_trial*self._p2*dl.dx, tensor = self._H_nln_lin)

    def _set_nonlinear_vector(self, u_vec = None, sym = True):
        if not u_vec is None:
            self._assign_vector(self._u.vector(), u_vec)
        (u1, u2) = dl.split(self._u)
        dl.assemble(u1**3*self._p1*dl.dx, tensor = self._d_nln_cub)
        if not self._substrate is None:
            dl.assemble(u1**2*self._substrate_func*self._p1*dl.dx, tensor=self._d_nln_quad)

    def assemble_operator(self, scale, sym = True):
        if sym:
            if self._H_linear_sym is None:
                self._set_linear_operator_sym(self.eps, self.sigma)
            self.H = self._H_linear_sym.copy()
            self.H.axpy(1. - scale, self._M_11, True)
            if not self._substrate is None:
                self.H.axpy(-scale*9*self.eta**2/32., self._M_11_ss, True)
        else:
            if self._H_linear_asym is None:
                self._set_linear_operator_asym(self.eps, self.sigma)
            self.H = self._H_linear_asym.copy()
            self.H.axpy(1. - scale, self._M_21, True)
            if not self._substrate is None:
                self.H.axpy(-scale*9*self.eta**2/32., self._M_21_ss, True)

        self.H.axpy(scale - 3., self._H_nln_quad, False)
        if not self._substrate is None:
            self.H.axpy(-1.5*self.eta, self._H_nln_lin, False)

        return self.H
        
    def assemble_preconditioner(self):
        self.P = self._M_11.copy()
        self.P.axpy(-self.eps**2, self._K_21, False)
        self.P.axpy(1./self.sigma, self._K_12, False)
        self.P.axpy(1., self._M_12, False)
        self.P.axpy(2*self.eps/math.sqrt(self.sigma), self._K_22, True)
        return self.P

    def _assemble_substrate(self):
        dl.assemble(self._substrate_func * self._u1_trial * self._p1 * dl.dx, tensor=self._M_11_ss)
        dl.assemble(self._substrate_func * self._u1_trial * self._p2 * dl.dx, tensor=self._M_21_ss)
        self._u1_c_ss = dl.assemble(self._substrate_func * self._p1 * dl.dx)
        self._u2_c_ss = dl.assemble(self._substrate_func * self._p2 * dl.dx)
        # self._vol_sub = dl.assemble(self._substrate_func * dl.dx(Vh[STATE].mesh()))
        self._vol_sub = dl.assemble(self._substrate_func * dl.dx)

    def gradient(self, u, sym = True):

        self._gradient.zero()
        if sym:
            self._gradient.axpy(1./self.sigma, self._K_22*u)
            self._gradient.axpy(1., self._M_21*u)
            self._gradient.axpy(-self.u1_mean, self._u2_c)
        else:
            self._gradient.axpy(1./self.sigma, self._K_12*u)
            self._gradient.axpy(1., self._M_11*u)
            self._gradient.axpy(-self.u1_mean, self._u1_c)

        return self._gradient

    def h1_norm(self, g):

        self._solver4gnorm.solve(self._help, g)

        return np.sqrt(g.inner(self._help))

    def poisson_solve(self, w, u, incremental = False):
    
        if not incremental:
            self._assign_vector(self._u.vector(), u)
            dl.assign(self._func_sub, self._u.sub(0))
            self._solver4poisson.solve(w, self._func_sub.vector())
        else:
            self._assign_vector(self._du.vector(), u)
            dl.assign(self._func_sub, self._du.sub(0))
            self._solver4poisson.solve(w, self._func_sub.vector())
        
    def u2_solve(self, u, set_nonlinear_vector = True):
    
        if set_nonlinear_vector:
            self._set_nonlinear_vector(u)
        self._assign_vector(self._u.vector(), u)
        self._u2_solve_rhs.vector().zero()
        self._u2_solve_rhs.vector().axpy(1., self._d_nln_cub)
        self._u2_solve_rhs.vector().axpy(-1., self._M_11*u)
        if not self._substrate is None:
            self._u2_solve_rhs.vector().axpy(0.75*self.eta, self._d_nln_quad)
            self._u2_solve_rhs.vector().axpy(-0.75*self.eta, self._u1_c_ss)
        self._u2_solve_rhs.vector().axpy(self.eps**2, self._K_11*u)
        dl.assign(self._func_sub, self._u2_solve_rhs.sub(0))
        self._solver4u2.solve(self._u2.vector(), self._func_sub.vector())
        dl.assign(self._u.sub(1), self._u2)
        self._assign_vector(u, self._u.vector())

    def grad_du(self, u, w, du):
    
        self._assign_vector(self._du.vector(), du)
        d1 = du.inner(self._M_12*u)
        dl.assign(self._func_sub, self._du.sub(0))
        d2 = self.sigma*self._func_sub.vector().inner(self._M_sub*w)
        
        return d1+d2

    def cost(self, u, w = None, set_nonlinear_vector = True):
    
        if set_nonlinear_vector:
            self._set_nonlinear_vector(u)
        if w is None:
            self.poisson_solve(self._help_sub, u)
            self.ok_energy[2] = 0.5*self.sigma*self._help_sub.inner(self._K_sub*self._help_sub)
        else:
            self.ok_energy[2] = 0.5*self.sigma*w.inner(self._K_sub*w)
            
        self.ok_energy[0] = 0.25*(self._d_nln_cub.inner(u) - 2*u.inner(self._M_11*u) + self._vol)
        self.ok_energy[1] = 0.5*self.eps**2*u.inner(self._K_11*u)
        if not self._substrate is None:
            self.ok_energy[3] = 0.25*self.eta*(self._d_nln_quad.inner(u)\
                                           - 3*u.inner(self._u1_c_ss) + 2*self._vol_sub)
        return np.sum(self.ok_energy)

    def symmetric_systems(self, scale):

        return [4*self.eps*math.sqrt(self.sigma) <= 1.- scale[i] for i in range(scale.size)]

    def incomp_res(self, u):
        return np.abs(u.inner(self._u1_c)/self._vol - self.u1_mean)

    def extract_sub_vector(self, u_sub, u, component):
        self._assign_vector(self._u.vector(), u)
        dl.assign(self._func_sub, self._u.sub(component))
        self._assign_vector(u_sub, self._func_sub.vector())
    
    def save(self, u, label):
        self._assign_vector(self._u.vector(), u)
        self._file_u1 << (self._u.split()[0], label)


# Author: Lianghao Cao
# Date: 05/06/2021

from __future__ import absolute_import, division, print_function

import math
import numpy as np
import dolfin as dl
import hippylib as hl
from scipy.special import erf
import matplotlib.pyplot as plt

plt.rc('text', usetex=True)
plt.rc('font', family='serif')
        
dl.parameters['linear_algebra_backend'] = 'PETSc'
dl.parameters["form_compiler"]["optimize"]     = True
dl.parameters["form_compiler"]["cpp_optimize"] = True
dl.parameters["form_compiler"]["representation"] = "uflacs"
dl.parameters["form_compiler"]["cpp_optimize_flags"] = "-O3 -ffast-math -march=native"

class poisson_solve():
    """
    This is a class for solving the possion equation with homogeneous Neumann boundary using a constrained minimization approach. See the following for detail.
        Bochev, P., & Lehoucq, R. B. (2005). On the finite element solution of the pure Neumann problem. SIAM Review, 47(1), 50–66. https://doi.org/10.1137/S0036144503426074
    """
    def __init__(self, Vu):
        """
        Constructor
        Inputs:
        - :code:`Vu`:                               The mixed Finite element space on which the state vector is defined.
        """
        Uh = Vu.extract_sub_space([0]).collapse()

        R = dl.FiniteElement("R", Uh.mesh().ufl_cell(), 0)
        ME = dl.MixedElement([Uh.ufl_element(),R])
        Zh = dl.FunctionSpace(Uh.mesh(),ME)
        
        self.w_temp = dl.Function(Uh)
        self.z_temp = dl.Function(Zh)
        self.z_test = dl.TestFunction(Zh)

        z_trial = dl.TrialFunction(Zh)
        (w_trial, lam_trial) = dl.split(z_trial)
        (w_test, lam_test) = dl.split(self.z_test)
        self._M_11 = dl.assemble(w_trial*w_test*dl.dx)
        A = dl.PETScMatrix()
        a_varf = dl.inner(dl.grad(w_trial), dl.grad(w_test))*dl.dx + w_trial*lam_test*dl.dx + lam_trial*w_test*dl.dx
        dl.assemble(a_varf, tensor = A)
        
        self.rhs, self.sol = dl.Vector(), dl.Vector()
        A.init_vector(self.rhs, 1)
        A.init_vector(self.sol, 1)
        self.solver = dl.PETScLUSolver(Vu.mesh().mpi_comm(), A)
        self.solver.parameters["symmetric"] = True
        
    def vector_U2Z(self, v, y):
        """
        Assemble the rhs of the Poisson equation that lies in the mixed space :code:`Zh`.
        Inputs:
        - :code:`v:                         A dolfin vector corresponds to subspace of :code:`Vu`.
        - :code:`y:                         A dolfin vector corresponds to space of :code:`Zh`.
        """
        y.zero()
        self.w_temp.vector().zero()
        self.w_temp.vector().axpy(1., v)
        dl.assign(self.z_temp.sub(0), self.w_temp)
        y.axpy(1., self._M_11*self.z_temp.vector())
        
    def vector_Z2U(self, v, y):
        """
        Extracting the vector corresponds to the first component of a function in :code:`Zh`.
        Inputs:
        - :code:`v:                         A dolfin vector corresponds to space of :code:`Zh`.
        - :code:`y:                         A dolfin vector corresponds to subspace of :code:`Vu`.
        """
        y.zero()
        self.z_temp.vector().zero()
        self.z_temp.vector().axpy(1., v)
        dl.assign(self.w_temp, self.z_temp.sub(0))
        y.axpy(1., self.w_temp.vector())
    
    def solve(self, v, y):
        """
        Solve the Poisson equation with input y and output v being vectors of functions in the subspace of :code:`Vu`.
        Inputs:
        - :code:`v:                         A dolfin solution vector corresponds to subspace of :code:`Vu`.
        - :code:`y:                         A dolfin rhs vector corresponds to subspace of :code:`Vu`.
        """
        self.vector_U2Z(y, self.rhs)
        self.solver.solve(self.sol, self.rhs)
        self.vector_Z2U(self.sol, v)

class project_state():
    """
    This is a class for L2 projection of mixed functions to a space where their first components have spatial average matches a given constant.
    """
    def __init__(self, Vu):
        """
        Constructor
        Inputs:
        - :code:`Vu`:                               The mixed Finite element space on which the state vector is defined.
        """
        u_trial = dl.TrialFunction(Vu)
        (u1_trial, u2_trial) = dl.split(u_trial)
        u_test = dl.TestFunction(Vu)
        (u1_test, u2_test) = dl.split(u_test)
        a_varf = u1_trial*u1_test*dl.dx + u2_trial*u2_test*dl.dx
        A = dl.PETScMatrix()
        dl.assemble(a_varf, tensor = A)
        solver = dl.PETScLUSolver(Vu.mesh().mpi_comm(), A)
        solver.parameters["symmetric"] = True
        
        self.z = dl.assemble(u1_test*dl.dx)
        self.c = dl.Vector(self.z)
        solver.solve(self.c, self.z)
        self.zT_c = self.z.inner(self.c)

    def set_mean(self, u1_mean):
        """
        Setting the target mean value for projection
        Inputs:
        - :code:`mean`:                         The target mean value.
        """
        self.m = u1_mean

    def project(self, y, v):
        """
        The projector function.
        Inputs:
        - :code:`y`:                            A dolfin vector that stores the projected function.
        - :code:`v`:                            A dolfin vector that stores the pre-projected function.
        """
        y.zero()
        zT_v = self.z.inner(v)
        y.axpy(-1.*zT_v/self.zT_c + self.m, self.c)
        y.axpy(1., v)

class GRF_initial_guess:
    """
    This is a class for generating Gaussian random fields (GRFs) with the covariance operator defined as the inverse of a elliptic differential operator
    """
    def __init__(self, Vu, correlation_length, scale):
        """
        Constructor
        Inputs:
        - :code:`Vu`:                               The mixed Finite element space on which the state vector is defined.
        - :code:`correlation_length`:               The correlation length of the Gaussian random fields
        - :code:`scale`:                            The pointwise upper and lower bound (+-scale) of the Gaussian random fields.
        """
        self.scale = scale
        self.mixed_function = dl.Function(Vu)
        order_parameter_space = Vu.extract_sub_space([0]).collapse()
        self.state_function = dl.Function(order_parameter_space)
        var = correlation_length/0.16
        delta = (var * correlation_length) ** (-0.5)
        gamma = delta*correlation_length**2

        self.initial_condition_distribution = hl.BiLaplacianPrior(order_parameter_space, gamma, delta, robin_bc=True)
        self.sample_vec = dl.Function(order_parameter_space).vector()
        
        self.noise_vec = dl.Vector()
        self.initial_condition_distribution.init_vector(self.noise_vec,"noise")
        
    def consume_random(self):
        """
        Consuming random seeds.
        """
        hl.parRandom.normal(1., self.noise_vec)

    def sample(self):
        """
        Return a Gaussian random field in the form of a state Vector in corresponds to the spcae :code:`Vh`.
        Outputs:
        (1) The dolfin vector for a Gaussian random field corresponds to the mixed space :code:`Vh`.
        """
        self.noise_vec.zero()
        hl.parRandom.normal(1., self.noise_vec)

        self.initial_condition_distribution.sample(self.noise_vec, self.sample_vec, add_mean=True)
        transformed_sample = self.scale*erf(self.sample_vec.get_local())
        self.state_function.vector().set_local(transformed_sample)
        self.mixed_function.vector().zero()
        dl.assign(self.mixed_function.sub(0), self.state_function)
        
        return self.mixed_function.vector()

def plot_iter_results(data, out_folder):
    """
    This is a function for plotting the iteration data of the energy stable Newton solver
    Inputs:
        - :code:`data`:                     An array of iteration data feeded by the solver
        - :code:`out_folder`:               The output folder for the plots. Assigned when constructing forward problem.
        """

    fontsize = 15
    plt.figure(1)
    plt.plot(data[:, 0])
    plt.xlabel(r"Iteration", fontsize = fontsize)
    plt.ylabel(r"Energy", fontsize = fontsize)
    plt.grid(True)
    plt.savefig(out_folder + "energy_evolution.pdf", bbox_inches = "tight")

    plt.figure(2)
    plt.semilogy(np.abs(np.diff(data[:,0])))
    plt.xlabel(r"Iteration", fontsize = fontsize)
    plt.ylabel(r"Energy descent", fontsize = fontsize)
    plt.grid(True)
    plt.savefig(out_folder + "energy_descent_evolution.pdf", bbox_inches = "tight")

    plt.figure(3)
    plt.semilogy(data[:,1])
    plt.xlabel(r"Iteration", fontsize = fontsize)
    plt.ylabel(r"Residual", fontsize = fontsize)
    plt.grid(True)
    plt.savefig(out_folder + "residual_evolution.pdf", bbox_inches = "tight")

    plt.figure(4)
    plt.semilogy(data[:,2])
    plt.xlabel(r"Iteration", fontsize = fontsize)
    plt.ylabel(r"Incompressibility residual", fontsize = fontsize)
    plt.grid(True)
    plt.savefig(out_folder + "incomp_residual_evolution.pdf", bbox_inches = "tight")

    plt.figure(5)
    plt.plot(data[:,3], "*")
    plt.xlabel(r"Iteration", fontsize = fontsize)
    plt.ylabel(r"Double well weight", fontsize = fontsize)
    plt.grid(True)
    plt.savefig(out_folder + "gamma_evolution.pdf", bbox_inches = "tight")

    plt.close('all')




        

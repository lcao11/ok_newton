# Author: Lianghao Cao
# Date: 05/06/2021

from __future__ import absolute_import, division, print_function

import math
import numpy as np
import dolfin as dl
import hippylib as hl
from .misc import plot_iter_results

STATE = 0
PARAMETER = 1

def LS_ParameterList():
    """
    Generate a ParameterList for line search globalization.
    type: :code:`LS_ParameterList().showMe()` for default values and their descriptions
    """
    parameters = {}
    parameters["c_armijo"] = [1e-4, "Armijo constant for sufficient reduction"]
    parameters["max_backtracking_iter"] = [50, "Maximum number of backtracking iterations"]

    return hl.ParameterList(parameters)

def EnergyStableNewton_ParameterList():
    """
    Generate a ParameterList for energy stable newton method.
    type: :code:`EnergyStableNewton_ParameterList().showMe()` for default values and their descriptions
    """
    parameters = {}
    parameters["rel_tolerance"] = [1e-10, "we converge when sqrt(g,g)/sqrt(g_0,g_0) <= rel_tolerance"]
    parameters["abs_tolerance"] = [1e-10, "we converge when sqrt(g,g) <= abs_tolerance"]
    parameters["gdm_tolerance"] = [1e-12, "we converge when (g,dm) <= gdm_tolerance"]
    parameters["maximum_iterations"] = [1000, "maximum number of iterations"]
    parameters["rescale_iterations"] = [3, "The total number of attempts to rescale Hessian to generate energy stable step, with minimum of 2."]
    parameters["print_level"] = [-1, "Control verbosity of printing screen"]
    parameters["LS"] = [LS_ParameterList(), "Sublist containing LS globalization parameters"]
    return hl.ParameterList(parameters)

class EnergyStableNewton:
    """
    The energy stable solver for Ohta-Kawasaki energy minimization prpblem.
    It is modified from FullNewtonSolver in (Peng Chen's modification of) hIPPYlib, a Newton solver to solve nonlinear PDE problems.
    The Newton system is solved either by LU solver or iterative solver, depending on the model parameter values.
    """
    termination_reasons = [
        "Maximum number of Iteration reached",  # 0
        "Norm of the gradient less than tolerance",  # 1
        "Maximum number of backtracking reached",  # 2
        "Norm of (g, dm) less than tolerance"  # 3
        ]

    def __init__(self, problem, parameters=EnergyStableNewton_ParameterList()):
        """
        Initialize the solver
        Type :code:`EnergySatbleSolver_ParameterList().showMe()` for list of default parameters
        and their descriptions.
        """
        self.problem = problem
        self.parameters = parameters
        self.it = 0
        self.converged = False
        self.reason = 0

        self.costValue = []
        self.costGrad = []
        self.costImcomp = []
        self.gamma = []
        self.stepLS = []
        self.ItLS = []
            
        self._lu_solver = dl.PETScLUSolver(problem.Vh[STATE].mesh().mpi_comm())
        self._lu_solver.parameters['symmetric'] = True
        self._gmres_solver = dl.PETScKrylovSolver("gmres")

        self.u_prev = self.problem.generate_state()
        self.dw = self.problem.generate_sub_state()
        self.w = self.problem.generate_sub_state()
        self.w_prev = self.problem.generate_sub_state()
        self.du = self.problem.generate_state()
        
    def _assign_vector(self,u1, u2):
        u1.zero()
        u1.axpy(1., u2)

    def solve(self, u, u0):
        """
        Solve the minimization problem with given initial guess ``u0" and return solution ``u".
        """
        rel_tol = self.parameters["rel_tolerance"]
        abs_tol = self.parameters["abs_tolerance"]
        gdu_tol = self.parameters["gdm_tolerance"]
        maximum_iterations = self.parameters["maximum_iterations"]
        print_level = self.parameters["print_level"]
        rescale_iterations = self.parameters["rescale_iterations"]

        c_armijo = self.parameters["LS"]["c_armijo"]
        max_backtracking_iter = self.parameters["LS"]["max_backtracking_iter"]

        self.it = 0
        self.converged = False

        scale = np.linspace(0,1, rescale_iterations)
        sym = self.problem.symmetric_systems(scale)

        P = self.problem.assemble_preconditioner()
    
        self._assign_vector(u, u0)
        self.problem.poisson_solve(self.w, u)
        cost_old = self.problem.cost(u, self.w)
        self.problem.u2_solve(u, set_nonlinear_vector=False)
        cost_new = cost_old

        grad = self.problem.gradient(u)
        gradnorm = self.problem.h1_norm(grad)
        grad_asym = None

        if (print_level >= 1):
            print("Initial residual norm: ", gradnorm)

        if self.problem._save:
            self.problem.save(u, self.it)

        self.costValue.append(cost_old)
        self.costGrad.append(gradnorm)
        self.costImcomp.append(self.problem.incomp_res(u))
        self.gamma.append(0.)
        self.stepLS.append(0.)
        self.ItLS.append(0.)

        while self.it < maximum_iterations:
            if (print_level >= 1):
                print("Iteration #", self.it)
            self.problem.set_nonlinear_operator(u)
            for i in range (rescale_iterations):
                if self.problem.iter_solve and (not sym[i]):
                    if grad_asym is None:
                        grad_asym = self.problem.gradient(u, sym = False)
                    H = self.problem.assemble_operator(scale[i], sym=False)
                    self._gmres_solver.set_operators(H, P)
                    self._gmres_solver.solve(H, self.du, -grad_asym)
                    if (print_level >= 1):
                        print("Solving for the Newton step with GMRES")
                else:
                    H = self.problem.assemble_operator(scale[i])
                    self._lu_solver.solve(H, self.du, -grad)
                    if (print_level >= 1):
                        print("Solving for the Newton step with Cholesky")
                grad_du = self.problem.grad_du(u, self.w, self.du)
                if (print_level >= 1):
                    print("gamma = ", 1.-scale[i], " grad_du = ", grad_du)
                if grad_du < 0.:
                    self.gamma.append(1.-scale[i])
                    break
            self.problem.poisson_solve(self.dw, self.du, incremental = True)

            alpha = 1.0
            descent = 0
            n_backtrack = 0
            self._assign_vector(self.u_prev, u)
            self._assign_vector(self.w_prev, self.w)

            while descent == 0 and n_backtrack < max_backtracking_iter:
            
                u.axpy(alpha, self.du)
                self.w.axpy(alpha, self.dw)
                cost_new = self.problem.cost(u, self.w)
                
                if (print_level >= 1):
                    print("n_backtrack = ", n_backtrack, "energy = ", cost_new)

                # Check if armijo conditions are satisfied
                if (cost_new < cost_old + alpha * c_armijo * grad_du):
                    cost_old = cost_new
                    descent = 1
                else:
                    n_backtrack += 1
                    self._assign_vector(u, self.u_prev)
                    self._assign_vector(self.w, self.w_prev)
                    alpha *= 0.5

            self.problem.u2_solve(u, set_nonlinear_vector = False)

            grad = self.problem.gradient(u)
            gradnorm = self.problem.h1_norm(grad)
            grad_asym = None

            if (print_level >= 1):
                print("Residual norm: ", gradnorm)

            self.costValue.append(cost_old)
            self.costGrad.append(gradnorm)
            self.costImcomp.append(self.problem.incomp_res(u))
            self.stepLS.append(alpha)
            self.ItLS.append(n_backtrack)

            if self.problem._save:
                self.problem.save(u, self.it)

            self.it += 1

            if descent == 0:
                self.converged = False
                self.reason = 2
                break

            # check if solution is reached
            if gradnorm < abs_tol:
                self.converged = True
                self.reason = 1
                break
            
            if abs(grad_du) < gdu_tol:
                self.converged = True
                self.reason = 3
                break
        
        if self.it == maximum_iterations:
            self.converged = False
            self.reason = 0

        if print_level >= 2:
            iter_data = np.zeros((len(self.costValue), 6))
            iter_data[:, 0] = np.array(self.costValue)
            iter_data[:, 1] = np.array(self.costGrad)
            iter_data[:, 2] = np.array(self.costImcomp)
            iter_data[:, 3] = np.array(self.gamma)
            iter_data[:, 4] = np.array(self.stepLS)

            np.save(self.problem.out_folder + "iter_data.npy", iter_data)
            plot_iter_results(iter_data, self.problem.out_folder)

        return self.converged, self.reason


#Author: Lianghao Cao
#Date: 05/06/2021

from __future__ import absolute_import, division, print_function

import numpy as np
import dolfin as dl
import hippylib as hl
from .EnergyStableNewton import EnergyStableNewton
from .ok_min_forms import ok_min_forms
from .misc import project_state

STATE = 0
PARAMETER = 1

class ok_min_prob(hl.PDEVariationalProblem):
    def __init__(self, Vh, state_init, substrate = None, save=False, out_folder="./", iter_solve = False):
        """
        Constructor
        Inputs:
        - :code:`Vh`:                               The list consists of the (mixed) state space and the parameter space.
        - :code:`state_init`:                       The class for generating random initial states.
        - :code:`substrate`:                        The substrate interaction strength field as a dolfin vector in the order parameter space
        - :code:`save`:                             Option for data saving.
        - :code:`out_folder`:                       Option for the location of data saving.
        """
        self._Vu_sub = Vh[STATE].extract_sub_space([0]).collapse()
        self.ok_min = ok_min_forms(Vh, substrate, save, out_folder, iter_solve)
        super(ok_min_prob, self).__init__(Vh, self.ok_min.varf_handler, [], [], is_fwd_linear=False)
        
        self.ok_min_solver = EnergyStableNewton(self.ok_min)
        
        self.project_state = project_state(Vh[STATE])
        self.state_init = state_init
        self.u0 = self.generate_state()

        self._func = dl.Function(Vh[STATE])
        self._func_sub = dl.Function(self._Vu_sub)

    def set_initial_condition(self, u):
        """
        Set random initial guesses, with the first component project to a re-assign spatial averagev alue.
        Inputs:
        - :code:`u`:                         The input vector that will be replaced with the generated initial guess.
        """
        self.project_state.project(u, self.state_init.sample())

        # Alternatively, one can use the following if the projection is not wanted.
        #u.zero()
        #u.axpy(1., self.u_init.sample())
        
    def solveFwd(self, state, x):
        """ Solve the OK energy minimization problem at the model parameter in :code:`x` and
        Inputs:
        - :code:`x`:                         A list consists of a state vector and a parameter vector.
        - :code:`state`:                     A state vector
        """
        self.project_state.set_mean(x[PARAMETER][2])
        self.set_initial_condition(self.u0)
        self.ok_min.set_parameters(x[PARAMETER])
        self.ok_min_solver.solve(state, self.u0)
        
    def consume_random(self):
        """
        Consume random initial guesses.
        """
        self.state_init.consume_random()

    def extract_order_parameter(self, op, state):
        """
        extract the order parameter from the state function in the mixed space.
        Inputs:
        - :code:`x`:                         A list consists of a state vector and a parameter vector.
        - :code:`order_parameter`:           The order parameter (dolfin vector) in the order parameter space.
        """
        self._func.vector().zero()
        self._func_sub.vector().zero()
        self._func.vector().axpy(1., state)
        dl.assign(self._func_sub, self._func.sub(0))
        op.zero()
        op.axpy(1., self._func_sub.vector())

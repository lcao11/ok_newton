# Direct minimization of OK energy

The numerical implementation of a globally convergent modified Newton method for the direct minimization of the Ohta&mdash;Kawasaki energy.

See the numerical scheme in the pre-print article:
 
<https://arxiv.org/abs/2010.15271>

The article is now published in SIAM Journal of Scientific Computing.

<https://epubs.siam.org/doi/abs/10.1137/20M1378119>

## Installation

To run the code, you will need to install
 
1. FEniCS 2019.1, a finite element library. 
    <https://fenicsproject.org/download/>

2. hIPPYlib, a inverse problem Python library
    <https://hippylib.github.io/>
    
For a step-by-step instruction, see the link below:

<https://github.com/hippylib/hippylib/blob/master/INSTALL.md>

Note: Installing FEniCS through Anaconda and hIPPYlib through "pip" are highly recommended.

## Usage

See the example files on how to run the code.

1. run_example.py: an example of how to run the code to minimize the OK energy.

2. run_dsa.py: an example of how to run the code for the directed self-assembly problem.

## Note

1. The solver implementation is coded to be compatible with the hIPPYlib library. In particular, it is compatible with the forward problem in the Model class in hIPPYlib.
2. The repository is still under construction (02/20/2022).
 - The run_example.py is ready for use.
 - Currently refining the iterative solver for chemoepitaxy application.

## Contributing
Please contact [Lianghao Cao](mailto:lianghao@ices.utexas.edu) for discussions on possible collaboration.

## License
[MIT](https://choosealicense.com/licenses/mit/)